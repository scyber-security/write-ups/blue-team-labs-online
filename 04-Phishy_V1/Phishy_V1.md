# [Phishy v1](https://blueteamlabs.online/home/investigation/4)

#### Scenario
> You have been sent a phishing link - It is your task to investigate this website and find out everything you can about the site, the actor responsible, and perform threat intelligence work on the operator(s) of the phishing site.


---
## Questions:

#### 1. The HTML page used on securedocument.net is a decoy. Where was this webpage mirrored from, and what tool was used? (Use the first part of the tool name only)
>`61.221.12.26/cgi-sys/defaultwebpage.cgi, HTTrack`

This answer can be found by navigating all the way to the top level of the website: `securedocument.net` and viewing the page source.

![Copier](./images/01-copier.png)


#### 2. What is the full URL of the background image which is on the phishing landing page?
> `http://securedocument.net/secure/L0GIN/protected/login/portal/axCBhIt.png`

This can be found be viewing the CSS of the page, and looking at the `background` value for the `body` tag.

![BG Image](./images/02-Background_Image.png)


#### 3. What is the name of the php page which will process the stolen credentials?
> `jeff.php`

When looking at the page source for the page, the `action` of the form is set to `jeff.php`.

![Jeff](./images/03-Jeff.png)


#### 4. What is the SHA256 of the phishing kit in ZIP format? (Provide the last 6 characters)
> `fa5b48`

After going back up a few directories on the web server, you eventually come across a zip file by the name of **0ff1cePh1sh.zip** in a directory called `secure` under the root of the web server. Downloading it and running the command `sha256sum 0ff1cePh1sh.zip` gives the answer.

![The Sum](./images/04-sha256.png)


#### 5. What email address is setup to receive the phishing credential logs?
> `boris.smets@tfl-uk.co`

Taking a look at `jeff.php`, there's a variable declaration of `$recipient`.

![Boris](./images/05-email.png)


#### 6. What is the function called to produce the PHP variable which appears in the index1.html URL?
> `Date().getTime()`

After unzipping the archive, there's a *HTML* file named **index.html**. This contains some simple javascript which redirects the user to the **index1.html** page with the current time as a parameter.

![What's the time?](./images/06-time.png)


#### 7. What is the domain of the website which should appear once credentials are entered?
> `office.com`

When looking at **jeff.php**, there's some javascript at the bottom of the page which redirects the user to the real site.

![Office](./images/07-office.png)


#### 8. There is an error in this phishing kit. What variable name is wrong causing the phishing site to break? (Enter any of 4 potential answers)
> `pass1`

When looking at both **index1.html** and **jeff.php**, the names of the *email* and *password* in **index1.html** do not match up with values the PHP script is expecting as POST data.

![Error](./images/08-broken.png)
